<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/masuk', function () {
        return view('barang.barangmasuk.createmasuk');
    });
    Route::get('/index', function () {
        return view('barang.barangmasuk.index');
    });
    Route::get('/indexkeluar', function () {
        return view('barang.barangkeluar.indexkeluar');
    });
    Route::get('/keluar', function () {
        return view('barang.barangkeluar.createkeluar');
    });
    
    //RoutesBarang
    Route::resource('barang', 'BarangController');
    //
});
