<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangkeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangkeluar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tgl_klr');
            $table->string('nama_brg');
            $table->unsignedBigInteger('id_brg');
            $table->foreign('id_brg')->references('id')->on('barangmasuk')->onDelete('cascade');
            $table->string('bnyk_brg');
            $table->string('satuan');
            $table->string('hrg_stn');
            $table->string('jumlah_stn');
            $table->string('untuk');
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangkeluar');
    }
}
