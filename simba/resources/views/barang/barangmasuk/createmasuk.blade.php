@extends('layout.master')
@section('title')
Halaman Barang Masuk
@endsection

@section('content')

<form method="POST" action="/barang">
    @csrf

    <div class="form-group">
        <label>Tanggal Masuk</label>
        <input type="date" class="form-control" name="tgl_msk">
    </div>
    @error('tgl_msk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Nama Barang</label>
        <input type="text" class="form-control" name="nama_brg">
    </div>
    @error('nama_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Jenis Barang</label><br>
        <select  class="custom-select custom-select-lg mb-3" name="jenis_brg" id="jenis_brg">
            <option value="" selected disabled hidden>Pilih Jenis Barang</option>
            <option value="Barang Habis Pakai">Barang Habis Pakai</option>
            <option value="Barang Tidak Habis Pakai">Barang Tidak Habis Pakai</option>
        </select>
    </div>
    @error('jenis_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Jumlah Barang</label>
        <input type="number" class="form-control" name="stok_brg">
    </div>
    @error('stok_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Satuan</label>
        <input type="text" class="form-control" name="satuan">
    </div>
    @error('satuan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Harga Barang (Rp)</label>
        <input type="number" class="form-control" name="hrg_brg">
    </div>
    @error('hrg_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection