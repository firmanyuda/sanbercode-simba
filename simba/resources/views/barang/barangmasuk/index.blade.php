@extends('layout.master')
@section('title')
Rekapitulasi Barang Masuk
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush
@push ('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('content')

<a href="/masuk" class="btn btn-primary mb-3"> Tambah Barang </a>

<table table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tanggal Masuk</th>
            <th scope="col">Nama barang</th>
            <th scope="col">Jenis Barang</th>
            <th scope="col">Jumlah Barang</th>
            <th scope="col">Satuan</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($barang as $key => $item)
            <tr>
                <th>{{$key+1}}</th>
                <td>{{$item->tgl_msk}}</td>
                <td>{{$item->nama_brg}}</td>
                <td>{{$item->jenis_brg}}</td>
                <td>{{$item->stok_brg}}</td>
                <td>{{$item->satuan}}</td>
                <td> 
                    <form action="/barang/{{$item->id}}" method="post">
                    <a href="/barang/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/barang/{{$item->id}}/edit" class="btn btn-warning btn-sm">Restock</a>
                    <form action="/barang/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
                
            </tr>
            
        @empty
        <tr>
            <td>Data Barang Masih Kosong</td>
        </tr>
            
        @endforelse
    </tbody>
</table>


@endsection