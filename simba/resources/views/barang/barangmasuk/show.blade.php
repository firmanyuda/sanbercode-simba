@extends('layout.master')
@section('title')
Halaman Detail Barang
@endsection

@section('content')

<h1>{{$barang->nama_brg}}</h1><br>

<label>Tanggal Barang Masuk</label>
<p>{{$barang->tgl_msk}}</p>

<label>Jenis Barang</label>
<p>{{$barang->jenis_brg}}</p>

<label>Jumlah Stok Barang</label>
<p>{{$barang->stok_brg}}</p>

<label>Satuan Barang</label>
<p>{{$barang->satuan}}</p>

<label>Harga per Barang</label>
<p>Rp. {{$barang->hrg_brg}},00</p>

<label>Total Harga Barang</label>
<p>Rp. {{$barang->tot_brg}},00</p>

@endsection