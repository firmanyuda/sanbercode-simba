@extends('layout.master')
@section('title')
Halaman Restock Barang
@endsection

@section('content')

<form method="POST" action="/barang/{{$barang->id}}">
    @csrf
    @method('put')

    <div class="form-group">
        <label>Tanggal Masuk</label>
        <input type="date" class="form-control" name="tgl_msk">
    </div>
    @error('tgl_msk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Jumlah Barang Restock</label>
        <input type="number" class="form-control" name="stok_brg">
    </div>
    @error('stok_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection