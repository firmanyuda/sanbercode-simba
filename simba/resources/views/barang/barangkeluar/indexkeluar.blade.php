@extends('layout.master')
@section('title')
Rekapitulasi Barang Keluar
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush
@push ('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('content')

<table table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tanggal Keluar</th>
            <th scope="col">Nama barang</th>
            <th scope="col">ID barang</th>
            <th scope="col">Banyak barang</th>
            <th scope="col">Satuan</th>
            <th scope="col">Harga Satuan</th>
            <th scope="col">Jumlah Satuan</th>
            <th scope="col">Untuk</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        {{-- @forelse ($cast as $key => $item)
            <tr>
                <th>{{$key+1}}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td> 
                    <form action="/cast/{{$item->id}}" method="post">
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
                
            </tr>
            
        @empty
        <tr>
            <td>Data Barang Masih Kosong</td>
        </tr>
            
        @endforelse --}}
    </tbody>
</table>


@endsection