@extends('layout.master')
@section('title')
Halaman Barang Keluar
@endsection

@section('content')

<form method="POST" action="/barang">
    @csrf

    <div class="form-group">
        <label>Tanggal Keluar</label>
        <input type="date" class="form-control" name="tgl_klr">
    </div>
    @error('tgl_klr')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Nama Barang</label>
        <input type="text" class="form-control" name="nama_brg">
    </div>
    @error('nama_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Banyak Barang</label>
        <input type="text" class="form-control" name="bnyk_brg">
    </div>
    @error('stok_brg')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Satuan</label>
        <input type="text" class="form-control" name="satuan">
    </div>
    @error('satuan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Harga Satuan</label>
        <input type="text" class="form-control" name="hrg_stn">
    </div>
    @error('hrg_stng')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Jumlah Satuan</label>
        <input type="text" class="form-control" name="jumlah_stn">
    </div>
    @error('jumlah_stn')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Untuk</label>
        <input type="text" class="form-control" name="untuk">
    </div>
    @error('untuk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection