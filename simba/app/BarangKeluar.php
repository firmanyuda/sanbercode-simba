<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangKeluar extends Model
{
    //
    protected $table = "barangkeluar";
    protected $fillable = ["tgl_klr", "nama_brg", "bnyk_brg", "satuan", "hrg_stn", "jumlah_stn", "untuk", "users_id",];
}
