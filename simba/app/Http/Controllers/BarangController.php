<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use RealRashid\SweetAlert\Facades\Alert;


class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $barang = Barang::all();
        return view("barang.barangmasuk.index", compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $barang = Barang::all();

        return view('barang.barangmasuk.createmasuk', compact('barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'tgl_msk' => 'required',
                'nama_brg' => 'required',
                'jenis_brg' => 'required',
                'stok_brg' => 'required',
                'satuan' => 'required',
                'hrg_brg' => 'required',
            ]
        );
        $barang = new Barang;

        $barang->tgl_msk = $request->tgl_msk;
        $barang->nama_brg = $request->nama_brg;
        $barang->jenis_brg = $request->jenis_brg;
        $barang->stok_brg = $request->stok_brg;
        $barang->satuan = $request->satuan;
        $barang->hrg_brg = $request->hrg_brg;
        $barang->tot_brg = ($request->stok_brg) * ($request->hrg_brg);

        $barang->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan Barang');

        return redirect('/barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $barang = Barang::find($id);
        return view('barang.barangmasuk.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $barang = Barang::find($id);
        return view('barang.barangmasuk.restock', compact('barang'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
                'tgl_msk' => 'required',
                'stok_brg' => 'required',
            ]
        );
        $barang = Barang::find($id);

        $barang->tgl_msk = $request->tgl_msk;
        $barang->stok_brg = ($request->stok_brg) + ($barang->stok_brg);
        $barang->update();

        Alert::success('Berhasil', 'Barang Berhasil Direstock');

        return redirect('/barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $barang = Barang::find($id);
        $barang->delete();

        Alert::success('Berhasil', 'Berhasil Menghapus Barang');

        return redirect('/barang');
    }
}
