<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    public $timestamps = false;
    protected $table = "barangmasuk";
    protected $fillable = ["tgl_msk", "nama_brg", "jenis_brg", "stok_brg", "satuan", "hrg_brg", "tot_brg", "users_id",];
}
